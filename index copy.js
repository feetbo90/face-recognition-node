// import * as faceapi from 'face-api.js';
// const tensorflow = require('@tensorflow/tfjs-node');
const faceapi = require('face-api.js');
// const { canvas, faceDetectionNet, faceDetectionOptions, saveFile } = require('./commons')
const canvas = require('canvas')
const path = require('path')
const bodyParser = require('body-parser');
const multer = require('multer');
const fs = require('fs')
const fetch = require("node-fetch");
const jimp = require('jimp')
// patch nodejs environment, we need to provide an implementation of
// HTMLCanvasElement and HTMLImageElement
const { Canvas, Image, ImageData } = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })

const express = require('express');
const { resolve } = require('path');
const { rejects } = require('assert');
const app = express()
const port = 3000
app.use(bodyParser.json({ limit: '100mb' }));


// const QUERY_IMAGE = './hasil3.jpg'
const QUERY_IMAGE = './images/greyscale/Indina_1614057059630.jpg'

app.use(express.urlencoded({ extended: true }))
// parse JSON
app.use(express.json())

Promise.all([
  faceapi.nets.faceRecognitionNet.loadFromDisk(path.join(__dirname, './models')),
  faceapi.nets.faceLandmark68Net.loadFromDisk(path.join(__dirname, './models')),
  faceapi.nets.ssdMobilenetv1.loadFromDisk(path.join(__dirname, './models'))
])
.then(start)

const Storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, './images/verif-users');
  },
  filename(req, file, callback) {
    callback(null, `${file.originalname}`);
  },
});

const upload = multer({ storage: Storage });

app.post('/api/upload', upload.single('photo'), async (req, res) => {
  try {
    const item = req.body
    let greyscale = new Promise((resolve, reject) => {
      jimp.read('./images/verif-users/' + item.nip + '.jpg', async (err, image) => {
        if (err) reject('gagal');
        const date = Date.now()
        image.greyscale().write('./images/greyscale/' + item.nip + '_' + date + '.jpg');
        resolve({ location: './images/greyscale/' + item.nip + '_' + date + '.jpg' })
      });
    })

    greyscale
      .then((ress) => ress)
      .then(async (ress) => {
        const interval = setInterval(async () => {

          const labeledFaceDescriptors = await loadLabeledImages(item.nip)
          const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.4)

          const location = ress.location
          const image = await canvas.loadImage(location)
          const displaySize = { width: image.width, height: image.height }


          const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
          const resizedDetections = faceapi.resizeResults(detections, displaySize)
          const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))


          fs.unlinkSync(ress.location)
          fs.unlinkSync('./images/verif-users/' + item.nip + '.jpg')
          console.log(results)
          if (results.length) {
            const cari = JSON.stringify(results[0])
            console.log(results[0])
            if (cari.search('unknown') < 0) {
              res.status(200).json({
                message: 'berhasil!',
                user: results[0],
                status: true
              });

            } else {
              res.status(201).json({
                message: 'gagal!',
                status: false
              });
            }
          } else {
            res.status(201).json({
              message: 'gagal!',
              status: false
            });
          }
          clearInterval(interval)
        }, 3000);
      }).catch((err) => {
        console.log(err.message, 'ini err')
      })
  } catch (error) {
    res.status(201).json({
      message: 'error! ' + error.message,
      status: false
    });
  }

});

const errorHandling = (err, req, res, next) => {
  res.json({
    status: 'error',
    message: err.message,
  })
}
app.use(errorHandling)

app.listen(port, () => console.log(`Server running at
        http://localhost:${port}`))


async function start() {
  try {
    // load image 
    const image = await canvas.loadImage(QUERY_IMAGE)

    // // load semua image
    const labeledFaceDescriptors = await loadLabeledImages('ansori')
    console.log(labeledFaceDescriptors, 'ini labeledFaceDescriptors')
    // label semua data yang ada 
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.4)
    const displaySize = { width: image.width, height: image.height }
    const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
    results.forEach((result, i) => {
      const box = resizedDetections[i].detection.box
      console.log("ini result : " + result.toString())
      // const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
      // drawBox.draw(canvas)
    })

  } catch (e) {
    console.log("gagal cuk : " + JSON.stringify(e.message))
  }
}


function loadLabeledImages(nip) {
  const labels = [nip]
  return Promise.all(
    labels.map(async label => {
      const descriptions = []
      try {
        if (!fs.existsSync(`./images/${label}`)){
          return null
        }

        const files = await fs.readdir(`./images/${label}`)
        files.then((ress) => {

        })
        .catch((err) => {
          
        })

        files.forEach( async (file) => {

          const img = await canvas.loadImage(`./images/${label}/${file}`)
          console.log({ label: label, imageWidth: img.width })

          const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
          descriptions.push(detections.descriptor)
        });

        return new faceapi.LabeledFaceDescriptors(label, descriptions)

      } catch (e) {
        console.log(JSON.stringify(e.message))
        return null;
      }

    })
  )
}

