// import * as faceapi from 'face-api.js';
// const tensorflow = require('@tensorflow/tfjs-node');
const faceapi = require('face-api.js');
// const { canvas, faceDetectionNet, faceDetectionOptions, saveFile } = require('./commons')
const canvas = require('canvas')
const path = require('path')

const fetch = require("node-fetch");

// patch nodejs environment, we need to provide an implementation of
// HTMLCanvasElement and HTMLImageElement
const { Canvas, Image, ImageData } = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })

const express = require('express')
const app = express()
const port = 3001


const QUERY_IMAGE = './images/iqbal/image (9).jpg'

app.use(express.urlencoded({ extended: true }))
// parse JSON
app.use(express.json())

Promise.all([
    faceapi.nets.faceRecognitionNet.loadFromDisk(path.join(__dirname, './models')),
    faceapi.nets.faceLandmark68Net.loadFromDisk(path.join(__dirname, './models')),
    faceapi.nets.ssdMobilenetv1.loadFromDisk(path.join(__dirname, './models'))
]).then(start);
console.log(faceapi.nets)
// fetch(
  // "https://jsonplaceholder.typicode.com/todos/1"
  // faceapi.nets.faceRecognitionNet.loadFromUri('./models')
    // faceapi.nets.faceRecognitionNet.loadFromDisk(path.join(__dirname, './models')),

// )
//   .then((response) => response.json())
//   .then((data) => console.log(data))
//   .catch((err) => console.log(err));

// fetch(faceapi.nets.faceRecognitionNet.loadFromUri('/models')).then(
//   function(response) {
//     if(response.ok) {
//       console.log("berhasil")
//     } else {
//       return Promise.reject(response);
//     }
//   }
// )

const errorHandling = (err, req, res, next) => {
    res.json({
    status: 'error',
    message: err.message,
    })
}
    app.use(errorHandling)

app.listen(port, () => console.log(`Server running at
        http://localhost:${port}`))


async function start() {
  try {
    // load image 
    const image = await canvas.loadImage(QUERY_IMAGE)

    // // load semua image
    const labeledFaceDescriptors = await loadLabeledImages()
    console.log("ini result : ")

    // label semua data yang ada 
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.4)
    const displaySize = { width: image.width, height: image.height }
    const detections = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)
    const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
    results.forEach((result, i) => {
      const box = resizedDetections[i].detection.box
      console.log("ini result : " + result.toString())
      // const drawBox = new faceapi.draw.DrawBox(box, { label: result.toString() })
      // drawBox.draw(canvas)
    })

  } catch (e) {
    console.log("gagal cuk : " + JSON.stringify(e.message))
  }
}


function loadLabeledImages() {
    const labels = ['iqbal', 'ansori']
    return Promise.all(
      labels.map(async label => {
        const descriptions = []
        try{
          for (let i = 1; i <= 1; i++) {
            const img = await canvas.loadImage(`./images/${label}/${i}.jpg`)
            console.log(label)

            console.log("ini image width : " + img.width)

            // const img = await faceapi.fetchImage(`./images/${label}/${i}.jpg`)
            // const img = await faceapi.fetchImage(`https://raw.githubusercontent.com/WebDevSimplified/Face-Recognition-JavaScript/master/labeled_images/${label}/${i}.jpg`)
            const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
            // console.log("ini detection : " + JSON.stringify(detections))
            descriptions.push(detections.descriptor)
          }
          return new faceapi.LabeledFaceDescriptors(label, descriptions)
        } catch (e) {
          console.log(JSON.stringify(e.message))
          return null;
        }
  
        
      })
    )
}